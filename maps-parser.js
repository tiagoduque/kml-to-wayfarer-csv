const parseKML = require('parse-kml');
const { v4: uuidv4 } = require('uuid'); // GUID
const createCsvWriter = require('csv-writer').createObjectCsvWriter;

// get source file and config file from arguments
const myArgs = process.argv.slice(2);
const sourceFile = myArgs[0]; //source.kml
const configFile = myArgs[1]; //config.json

console.log('-- Reading config file --');
const configData = require(configFile);

// add the hexColor property to the config objects
configData.forEach( x => {
    x['hexColor'] = processRgbColorString(x.rgbColor);
});
// console.log(configData);

console.log('-- Parsing KML file --');

parseKML.toJson(sourceFile).then( data => {
    // console.log(data); // raw json
    const features = data.features;
    const entries = [];
    let potentialCount = 0;
    let liveCount = 0;
    let rejectedCount = 0;
    let submittedCount = 0;
    let senteditCount = 0;
    let potentialeditCount = 0;
    const today = new Date();
    const todayDate = today.getFullYear()+'/'+(today.getMonth()+1)+'/'+today.getDate();
    let configErrorUnknownColor = false;
    
    // unparsed json
    // console.log(features);

    features.forEach( f => {
        let mapStatus = '';
        let styleUrl = f.properties.styleUrl.split('-');
        let parsedStatusColor = '#' + styleUrl[2];
        parsedStatusColor = parsedStatusColor.toLowerCase();
        // console.log(parsedStatusColor);

        // lookup the entry's parsedStatusColor on config
        const configEntry = configData.find( c => {
            return c.hexColor === parsedStatusColor
        });
        // console.log(configEntry);

        // then assign the correct mapStatus
        if (configEntry !== undefined) {
            if (configEntry.wayfarerStatus !== '') {
                mapStatus = configEntry.wayfarerStatus.toLowerCase();;
            } else {
                mapStatus = 'potentialedit';
            }
        } else {
            configErrorUnknownColor = true;
        }
        // console.log(mapStatus);
        // console.log('-----------------------------------');

        switch (mapStatus) {
            case 'potential':
                potentialCount++;
                break;
            case 'submitted':
                submittedCount++;
                break;
            case 'live':
                liveCount++;
                break;
            case 'rejected':
                rejectedCount++;
                break;
            case 'potentialedit':
                potentialeditCount++;
                break;
            case 'sentedit':
                senteditCount++;
                break;
            
            default:
                //default behavior
                break;
        }

        // potential
        // submitted
        // live
        // rejected
        // potentialedit
        // sentedit

        entries.push(
            {
                id: uuidv4(),
                timestamp: todayDate,
                title: f.properties.name, 
                description: f.properties.description !== undefined ? f.properties.description : '' ,
                lat: f.geometry.coordinates[1],
                lng: f.geometry.coordinates[0],
                status: mapStatus,
                nickname: 'agentbiker28',
                submitteddate: '',
                responsedate: '',
                candidateimage: '',
                intellink: `https://intel.ingress.com/intel/?z=19&ll=${f.geometry.coordinates[1]},${f.geometry.coordinates[0]}`
            }
        );
    });

    // console.log(entries); // show all parsed and formatted entries
    console.log('TOTAL POI entries: ' + entries.length);

    // SHOW COUNTS ON EXECUTION
    console.log('potentialCount: ' + potentialCount);
    console.log('liveCount: ' + liveCount);
    console.log('rejectedCount: ' + rejectedCount);
    console.log('submittedCount: ' + submittedCount);
    console.log('senteditCount: ' + senteditCount);
    console.log('potentialeditCount: ' + potentialeditCount);
    if (configErrorUnknownColor) {
        console.log('!! WARNING !! Unknown color in .kml (Not configured on config.json) ');
    }

    // WRITE LIST OF POI TO CSV
    const csvWriter = createCsvWriter({
        path: 'out.csv',
        header: [
            {id: 'id', title: 'Id'},
            {id: 'timestamp', title: 'Timestamp'},
            {id: 'title', title: 'Title'},
            {id: 'description', title: 'Description'},
            {id: 'lat', title: 'Lat'},
            {id: 'lng', title: 'Lng'},
            {id: 'status', title: 'Status'},
            {id: 'nickname', title: 'Nickname'},
            {id: 'submitteddate', title: 'Submitteddate'},
            {id: 'responsedate', title: 'Responsedate'},
            {id: 'candidateimage', title: 'Candidateimage'},
            {id: 'intellink', title: 'Intellink'},
        ]
    });
    csvWriter.writeRecords(entries)
        .then(()=> console.log('-- The output CSV file was written successfully (out.csv) -- '));

    // console.log('end parse kml');
});


function componentToHex(c) {
  var hex = c.toString(16);
  return hex.length == 1 ? "0" + hex : hex;
}

function rgbToHex(r, g, b) {
  return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

function processRgbColorString(colorRGB) {
    // RGB string to HEX conversion
    // const colorRGB = 'RGB (2, 136, 209)';
    // const colorRGB = 'RGB(2, 136, 209)';
    // const colorRGB = 'RGB(2,136,209)';
    let colorHEX = '';

    let rgbSplit = colorRGB.replace(' ', '');
    rgbSplit = rgbSplit.replace('RGB(', '');
    rgbSplit = rgbSplit.replace(')', '');
        
    let rgbValuesStr = rgbSplit.split(',');

    const rgbValues = rgbValuesStr.map( x => parseInt(x, 10) );

    colorHEX = rgbToHex(rgbValues[0], rgbValues[1], rgbValues[2]);

    return colorHEX;
}
