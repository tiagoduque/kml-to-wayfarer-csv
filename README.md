# kml-to-wayfarer-csv

Convert exported map data (.kml) from Google My Maps to csv, allowing easy import into a Google Sheet to use with @AlfonsoML's [wayfarer planner](https://gitlab.com/AlfonsoML/wayfarer).

## Getting started (Windows)

Make sure you have installed Node on your computer.
Go to this link and download the LTS version [https://nodejs.org/](https://nodejs.org/)

After that, open a command prompt by typing "cmd" (click on windows search, on the right of the start menu) 

Navigate to your Downloads folder (for example)
```
cd C:\Users\[your_user]\Dowloads
``` 

Create a folder to run the project and enter it:
```
mkdir my-wayfarer
cd my-wayfarer
``` 

Download these three files into that folder:  
**package.json**  
**maps-parser.js**  
**config.json**  
(these files are on the list above. to download them, click on each of them and then click download as in the image below) 

<!-- ![alt text](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 1") -->
![alt text](assets/download-file-help.png "download help")

Install the **maps-parser.js** dependencies.  
On that same folder, type:
```
npm install
``` 

You are ready to start!

## Configuration

If you don't want any kind of distinction or categorization (potential, accepted, rejected) of your existing map data, you can skip this section and jump to the Usage section.
All your map data will be added with the default status "potentialedit" and marked as such when you import it into IITC.

If you want to maintain most of your existing map organization/categorization by colors, you can edit the configuration file named "config.json".

Let's first look at its structure:

```
[
  {
    "id": "1",
    "rgbColor": "RGB (136, 14, 79)",
    "wayfarerStatus": ""
  },
  {
    "id": "2",
    "rgbColor": "RGB (165, 39, 20)",
    "wayfarerStatus": ""
  },
  ...
  ...
]
```

The [  ] represents the list of color configurations available and the { } represents each configuration entry.
Each entry has three fields: *id*, *rgbColor* and *wayfarerStatus*.

- *id*: is just a counter for the entries and it has a maximum of 30 entries, that correspond to the 30 color choices in Google My Maps.  
**(this field should not be edited)**

- *rgbColor*: is the color code (RGB format) represented in each square of the color pallet on Google My Maps as the image below illustrates.  
**(this field should not be edited)**  

![alt text](assets/color-code.png "download help")

- *wayfarerStatus*: this is the only field you are going to edit and write the desired status for that place, if you want that color to have a match with a wayfarer status value which can be one of the following six:
    - potential
    - submitted
    - live
    - rejected
    - potentialedit
    - sentedit

  **Important:** You must enter one of these status text inside the "" right after "wayfarerStatus": (exactly as it is, in lower case)  
  If you accidentally delete any other characters this converter might not work.  
  Also, do not change the rgbColor values [ RGB (175, 180, 43) ] this will make the converter fail for that color, as it not exists on the Google My Maps color pallette.


For example, if I want the green markers to be assigned as "live" status.
All I have to do is look for the color code on Google My Maps color pallette. This is done by clicking the marker, click the style icon and then hover the mouse on top of it.
Then find that color inside the config.json file and edit the wayfarerStatus field of that color by adding the text "live".

 1. Find out the color code  

 ![alt text](assets/color-code.png "download help")
 
 2. Edit that color with the status you want
 ```
  {
    "id": "21",
    "rgbColor": "RGB (15, 157, 88)",
    "wayfarerStatus": "live"
  },
 ```

If you are not sure which status to assign to a certain color, leaving the wayfarerStatus field empty will automatically mark it with the default status "potentialedit" when you import it into IITC.



## Usage

Export all your locations from Google My Maps using KML format (map-file.kml)
[Not sure how? Click here](https://www.youtube.com/watch?v=I2XuII7fD2M)


Place that file on the same folder you made the setup, and run the following command:
```
node maps-parser.js map-file.kml config.json
``` 

If all went well, you should have an output like this

![alt text](assets/node-output.png "node output")

A file named out.csv will be created. 

This file can be opened on Excel or Libre/Open Office.  
Now, all you have to do is copy and paste the contents of the Google Sheets associated with Alfonso's Wayfarer plugin.


<!-- ## Important note -->

<!-- Currently this application only supports a fixed set of maps marker color set.

In the future I will include a configuration file to be easier to tell which color(s) will match with each wayfarer category (potential, submitted, live, rejected, potentialedit, sentedit). -->


## License

MIT